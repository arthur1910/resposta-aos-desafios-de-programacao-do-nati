
class QuestaoTabuleiro {

    public static void contPecas(String[][] tabuleiro) {
        // Matriz contendo as pe�as e os respectivos contdores
        String[][] pecasqtde = {
            {"Vazio", "0"},
            {"Pe�o", "0"},
            {"Bispo", "0"},
            {"Cavalo", "0"},
            {"Torre", "0"},
            {"Rainha", "0"},
            {"Rei", "0"}
        };

        for(int i = 0; i < tabuleiro.length; i++) {
            for(int j = 0; j < tabuleiro[i].length; j++) {
                int posicao = Integer.parseInt(tabuleiro[i][j]);
                pecasqtde[posicao][1] = Integer.toString(Integer.parseInt(pecasqtde[posicao][1]) + 1);
            }
        }
        for (int i = 1; i < pecasqtde.length; i++) {
        	System.out.println(pecasqtde[i][0] + ": " + pecasqtde[i][1]);
		}
        
    }

    public static void main(String[] args) {
        String[][] tabuleiro = { 
            { "4", "3", "2", "5", "6", "2", "3", "4" }, 
            { "1", "1", "1", "1", "1", "1", "1", "1" },
            { "0", "0", "0", "0", "0", "0", "0", "0" }, 
            { "0", "0", "0", "0", "0", "0", "0", "0" },
            { "0", "0", "0", "0", "0", "0", "0", "0" }, 
            { "0", "0", "0", "0", "0", "0", "0", "0" },
            { "1", "1", "1", "1", "1", "1", "1", "1" }, 
            { "4", "3", "2", "5", "6", "2", "3", "4" } 
        };

        contPecas(tabuleiro);
    }
}
